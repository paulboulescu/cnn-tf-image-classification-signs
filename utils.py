
import numpy as np
import h5py
import math


def random_mini_batches(x, y, mini_batch_size=2048, type="fc"):
    """
    Creates a list of random minibatches from (X, Y)

    Arguments:
    x = input data to be split into mini-batches
    y = list of 1 and 0
    mini_batch_size = size of the mini-batches
    type = 'fc' for [n, m] / 'cn' for [m, nh, nw, nc]

    Returns:
    mini_batches = list of tuples (mini_batch_X, mini_batch_Y)
    """

    # Number of training examples
    if type == "fc":
        m = x.shape[1]
    elif type == "cn":
        m = x.shape[0]
    mini_batches = []

    # Shuffle (X, Y)
    permutation = list(np.random.permutation(m))

    if type == "fc":
        shuffled_x = x[:, permutation]
        # Previous formula, was causing an error in TF:
        # shuffled_y = y[:, permutation].reshape((1, m))
        shuffled_y = y[:, (permutation)].reshape((y.shape[0], m))
    elif type == "cn":
        shuffled_x = x[permutation, :, :, :]
        shuffled_y = y[permutation, :]

    # Number of mini batches of size mini_batch_size in partitionning
    num_complete_minibatches = math.floor(m / mini_batch_size)
    for k in range(0, int(num_complete_minibatches)):
        if type == "fc":
            mini_batch_x = shuffled_x[:, k * mini_batch_size:(k + 1) * mini_batch_size]
            mini_batch_y = shuffled_y[:, k * mini_batch_size:(k + 1) * mini_batch_size]
        elif type == "cn":
            mini_batch_x = shuffled_x[k * mini_batch_size: k * mini_batch_size + mini_batch_size, :, :, :]
            mini_batch_y = shuffled_y[k * mini_batch_size: k * mini_batch_size + mini_batch_size, :]
        mini_batch = (mini_batch_x, mini_batch_y)
        mini_batches.append(mini_batch)

    # Handling the end case, when last mini-batch < mini_batch_size
    if m % mini_batch_size != 0:
        if type == "fc":
            mini_batch_x = shuffled_x[:, num_complete_minibatches * mini_batch_size:m]
            mini_batch_y = shuffled_y[:, num_complete_minibatches * mini_batch_size:m]
        elif type == "cn":
            mini_batch_x = shuffled_x[num_complete_minibatches * mini_batch_size: m, :, :, :]
            mini_batch_y = shuffled_y[num_complete_minibatches * mini_batch_size: m, :]
        mini_batch = (mini_batch_x, mini_batch_y)
        mini_batches.append(mini_batch)

    return mini_batches


def one_hot_matrix(y, c):
    y = np.eye(c)[y.reshape(-1)].T
    return y


def load_dataset(train_dataset_path, test_dataset_path):

    train_dataset = h5py.File(train_dataset_path, "r")
    train_set_x_orig = np.array(train_dataset["train_set_x"][:])  # train set features
    train_set_y_orig = np.array(train_dataset["train_set_y"][:])  # train set labels

    test_dataset = h5py.File(test_dataset_path, "r")
    test_set_x_orig = np.array(test_dataset["test_set_x"][:])  # your test set features
    test_set_y_orig = np.array(test_dataset["test_set_y"][:])  # your test set labels

    classes = np.array(test_dataset["list_classes"][:])  # the list of classes

    train_set_y_orig = train_set_y_orig.reshape((1, train_set_y_orig.shape[0]))
    test_set_y_orig = test_set_y_orig.reshape((1, test_set_y_orig.shape[0]))

    return train_set_x_orig, train_set_y_orig, test_set_x_orig, test_set_y_orig, classes