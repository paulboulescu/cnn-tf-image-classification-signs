# CNN Image Classifier with TensorFlow

## About
Convolutional Neural Network model with TensorFlow, used to classify images of 6 hand signs, corresponding to numbers from 0 to 5.

## Instalation
1. Clone the repository
```
$ git clone https://paulboulescu@bitbucket.org/paulboulescu/cnn-tf-image-classification-signs.git
```

2. Train and Test
```
$ python model.py
```

## Requirements
1. TensorFlow

## Details
* Model: CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> FLATTEN -> FULLYCONNECTED -> SOFTMAX
* Train Accuracy: **0.962963**
* Test Accuracy: **0.816666**
* Epochs: **100**
* MiniBatch: **64**
* Learning Rate: **0.009**
* Optimizer: **Adam**
* Initializer: **Xavier**
* Loss: **Softmax / Cross Entropy**

## Disclaimer
Created under the Deep Learning Specialization (Andrew Ng, Younes Bensouda Mourri, Kian Katanforoosh) - Coursera. The code uses ideas, datasets, algorithms, and code fragments presented in the Course.